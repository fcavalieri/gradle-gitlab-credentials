# Gradle Gitlab Credentials

## Usage

settings.gradle
```groovy
pluginManagement {
    repositories {
        maven { url "https://gitlab.com/api/v4/projects/33639670/packages/maven" }
        gradlePluginPortal()
    }
}
```

build.gradle
```groovy
plugins {
    id "com.fcavalieri.gradle.gitlabcredentials" version 'X.X.X'
}
```

Usage:

```groovy
task mytask {
    doLast {
        gitLabCredentials.printCredentials()
    }
}
```

```groovy
repositories {
    mavenLocal()
    mavenCentral()
    maven {
        url = uri('https://gitlab.com/api/v4/projects/16478731/packages/maven') //JInterval
        credentials(HttpHeaderCredentials, new Action<HttpHeaderCredentials>() {
            @Override
            void execute(HttpHeaderCredentials credentials) {
                setGitLabCredentials(credentials);
            }
        })
        authentication {
            header(HttpHeaderAuthentication)
        }
    }
}
```

```groovy
import java.nio.file.Files
task publishTP {
   dependsOn "packageTP"
   doLast {
      def url = new URL("https://gitlab.com/api/v4/projects/" + projectId + "/packages/generic/" + project.name +  "/" + project.version + "/" + packageFileName)
      def req = url.openConnection()
      req.setDoOutput(true)
      req.setRequestMethod('PUT')
      gitLabCredentials.apply(req)
      Files.copy(tasks.getByName("packageTP").getOutputs().getFiles().getFiles()[0].toPath(), post.outputStream)
      def postRC = post.getResponseCode();
      if (!postRC.equals(201)) {
         throw new GradleException("Upload failure: " + post.getInputStream().getText())
      }
   }
}
```
