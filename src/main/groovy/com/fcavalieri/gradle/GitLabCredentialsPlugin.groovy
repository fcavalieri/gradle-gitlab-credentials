package com.fcavalieri.gradle

import org.gradle.api.GradleException
import org.gradle.api.Project
import org.gradle.api.Plugin
import org.gradle.api.credentials.HttpHeaderCredentials
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class GitLabCredentialsPlugin implements Plugin<Project> {
    void apply(Project project) {
        project.extensions.create("gitLabCredentials", GitLabCredentialsExtension, project)
        project.tasks.register("gitLabCredentials") {
            doLast { new GitLabCredentialsExtension(project).print() }
        }
    }
}

class GitLabCredentialsExtension {
    String name = null
    String value = null
    Project project = null
    static final Logger logger = LoggerFactory.getLogger("FooPlugin")

    GitLabCredentialsExtension(Project project) {
        this.project = project
    }

    void read() {
        if (name != null && value != null)
            return

        if (project.properties.get("gitLabPrivateToken")) {
            name = "Private-Token"
            value = project.properties.get("gitLabPrivateToken")
            logger.info("Gitlab Credentials: using property gitLabPrivateToken")
        } else if (project.properties.get("gitLabDeployToken")) {
            name = "Deploy-Token"
            value = project.properties.get("gitLabDeployToken")
            logger.info("Gitlab Credentials: using property gitLabDeployToken")
        } else if (project.properties.get("gitLabJobToken")) {
            name = "Job-Token"
            value = project.properties.get("gitLabJobToken")
            logger.info("Gitlab Credentials: using property gitLabJobToken")
        } else if (System.getenv("PRIVATE_TOKEN") != null) {
            name = "Private-Token"
            value = System.getenv("PRIVATE_TOKEN")
            logger.info("Gitlab Credentials: using PRIVATE_TOKEN environment variable")
        } else if (System.getenv("DEPLOY_TOKEN") != null) {
            name = "Deploy-Token"
            value = System.getenv("DEPLOY_TOKEN")
            logger.info("Gitlab Credentials: using DEPLOY_TOKEN environment variable")
        } else if (System.getenv("CI_JOB_TOKEN") != null) {
            name = "Job-Token"
            value = System.getenv("CI_JOB_TOKEN")
            logger.info("Gitlab Credentials: using CI_JOB_TOKEN environment variable")
        } else {
            throw new GradleException("No GitLab credentials defined")
        }
    }

    void apply(HttpHeaderCredentials credentials) {
        read()
        credentials.name = name
        credentials.value = value
    }

    void apply(URLConnection connection) {
        read()
        connection.setRequestProperty(name, value)
    }

    void print() {
        read()
        println(name + ": " + value)
    }
}
